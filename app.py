import keras
from keras.layers.convolutional import Conv2D,MaxPooling2D
from keras.layers.core import Dense,Activation,Flatten,Dropout
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator
import os

def createModel():
    #load sequential model
    model = Sequential()
    #convolution layer
    model.add(Conv2D(32, (3, 3), input_shape=(64,64,3)))
    model.add(Activation('relu'))
    #max pooling layer
    model.add(MaxPooling2D(pool_size=(2, 2)))

    #conv layer
    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    #max pooling
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #conv
    model.add(Conv2D(128, (3, 3)))
    model.add(Activation('relu'))
    #max pooling
    model.add(MaxPooling2D(pool_size=(2, 2)))

    #convert output of maxpooling to 1d vector
    model.add(Flatten())
    #fully connected layer
    model.add(Dense(1024,activation='relu'))
    model.add(Dense(512,activation='relu'))
    #output layer
    model.add(Dense(7,activation='softmax'))
    if (os.path.exists('weights.h5')):
        model.load_weights('weights.h5')
    model.compile(loss=keras.losses.categorical_crossentropy,optimizer='adam' ,metrics=['accuracy'])

    return model
if __name__=="__main__":
    model=createModel()
    #data augmentation
    train_generator=ImageDataGenerator( rescale=1./255,shear_range=0.2,zoom_range=0.2,horizontal_flip=True)
    train_generator = train_generator.flow_from_directory('data/train',target_size=(64,64),batch_size=4,class_mode='categorical')
    #data augmentation
    test_generator=ImageDataGenerator(rescale=1./255)
    test_generator = test_generator.flow_from_directory('data/test', target_size=(64,64),batch_size=4,class_mode='categorical')
    model.fit_generator(train_generator, steps_per_epoch=100, epochs=50, validation_data=test_generator,validation_steps=100)
    model.save_weights('weights.h5')
    model_scores = model.evaluate_generator(test_generator)
    print("Accuracy: %.2f%%" % (model_scores[1])*100)
