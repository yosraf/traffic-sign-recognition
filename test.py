import random

import numpy as np
from app import createModel
from keras.preprocessing.image import img_to_array
import os
import tkinter as tk
import cv2
from PIL import Image,ImageTk


img = None
canvas=None
canvas_image=None
label=None
indice=0
img_path=""
image_folder = './validation/'

def predict_main():

    global img
    #img = cv2.imread('test5.jpg')
    img = cv2.resize(img, dsize=(64, 64))
    x = img_to_array(img)
    x = x.reshape((1,) + x.shape)
    npimg = np.array(x)

    predict = model.predict_classes(npimg)

    print(predict)
    label['text']=classes[predict[0]]
    #return predict[0]






def select_first(path):
    global img
    img = cv2.imread(path)


def goNext():
    global indice
    global img_path
    global img
    global canvas_image
    if(indice<_len-1):
        indice=indice+1
    else:
        indice=0
    print(indice)
    img_path = image_folder + list_imgs[indice]
    img = cv2.imread(img_path)
    print(indice)
    print(img_path)
    bmpimg = Image.open(img_path)
    photo=ImageTk.PhotoImage(image=bmpimg,master=wind)
    canvas.itemconfig(canvas_image,image=photo)
    canvas_image.image=photo
    pass
if __name__ == '__main__':
    model = createModel()
    classes=['danger ahead','deer crossing','slippery road','speed limit 30','speed limit 40','speed limit 50','stop']
    list_imgs=os.listdir(image_folder)
    random.shuffle(list_imgs)
    _len=len(list_imgs)
    img_path=image_folder+list_imgs[indice]
    print(_len)
    bmpimg=Image.open(img_path)
    select_first(img_path)
    wind=tk.Tk()
    label=tk.Label(text="")
    label.grid(row=0,column=0)
    bmpphoto = ImageTk.PhotoImage(image=bmpimg,master=wind)
    canvas=tk.Canvas(wind,width=550,height=550)
    canvas.grid(row=1, column=0)
    canvas_image=canvas.create_image((125,125),image=bmpphoto,anchor='nw')
    next=tk.Button(text="Next",command=goNext)
    next.grid(row=2,column=0)
    predict = tk.Button(text="predict", command=predict_main)
    predict.grid(row=2, column=1)

    wind.mainloop()
    """
     for n, image_file in enumerate(os.scandir(image_folder)):
        img = image_file
        fig, ax = plt.subplots(1)
        mngr = plt.get_current_fig_manager()
        mngr.window.setGeometry(250, 120, 800, 600)
        image = cv2.imread(image_file.path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        ax.imshow(image)
        img = image

        key = plt.connect('key_press_event', onkeypress)
        plt.show()
    """








